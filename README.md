# Prime Finder

## Authors

- Eli W. Hunter

## Installation

Clone the repository and then run `PrimeFinder.py`.

```shell
git clone https://gitlab.com/elihunter173/prime-finder.git
cd prime-finder
python PrimeFinder.py
```

## Technique

1. initiate list of primes
2. see if tested number is divisible by any listed primes
3. if yes, not prime. else, prime

## Built With

- [Python](https://www.python.org/) - A simple, whitespace based, object-oriented programming language.
