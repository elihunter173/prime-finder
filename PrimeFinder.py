"""
Finds the number of primes specified.

Usage:
python PrimeFinder.py NUMBER_OF_PRIMES
Where NUMBER_OF_PRIMES is the number of primes to find.
"""

import sys
import math


def is_prime(test_number, found_primes):
    """Determine if n is prime given a list of all primes less than n.

    Args:
        test_number is the number being tested to see if it's prime
        found_primes is the list of all numbers which are less than test_number
        and prime
    """
    for current_prime in found_primes:
        # This is the dropout condiiton.
        if current_prime > math.sqrt(test_number):
            return True
        if test_number % current_prime == 0:  # tests for factors
            return False

    # if found_primes is empty, just return true.
    return True


def find_primes(primes_to_find):
    """Find primes up to primes_to_find.

    This method finds primes by starting with an empty list of primes and then
    testing if the testing-number has any factors inside of the list of found
    primes.

    Args:
        primes_to_find is the number of primes to find.
    """
    found_primes = []
    test_number = 2  # the first prime

    while len(found_primes) < primes_to_find:
        if is_prime(test_number, found_primes):
            found_primes.append(test_number)
        test_number += 1
    return found_primes


# The first arg is the name of program
if len(sys.argv) != 2:
    print("Usage Error:")
    print("  python PrimeFinder.py NUMBER_OF_PRIMES")
    print("  Where NUMBER_OF_PRIMES is the number of primes to find.")
    exit(1)  # exit with error

primes_to_find = int(sys.argv[1])  # number of primes to find

primes_file = open("primes.txt", "w")  # opens primes.txt file for writing
primes_file.write("First " + str(primes_to_find) + " primes:\n")
primes_file.write(str(find_primes(primes_to_find)))  # writes primes to primes.txt
primes_file.close()  # closes file
